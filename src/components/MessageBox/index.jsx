/* eslint-disable */
import React, { useEffect} from "react";
import { MessageType } from "../../util/enum";
import { Icon } from "@iconify/react";

const MessageBox = ({ message, type, setMessage }) => {
  useEffect(() => {
    
    if (message) {
      setTimeout(() => {
        setMessage(null);
      }, 5000);
    }
  }, [message, setMessage]);
  const renderIcon = () => {
    if (type === MessageType.ERROR) {
      return <Icon icon="ion:alert-circle-outline" width="20" />;
    } else if (type === MessageType.SUCCESS) {
      return <Icon icon="ic:outline-check-circle" width="20" />;
    }
  };

  return (
    message && (
      <div
        className={type === MessageType.SUCCESS ? "success-box" : "error-box"}
      >
        {renderIcon()} <>{message}</>
      </div>
    )
  );
};

export default MessageBox;
