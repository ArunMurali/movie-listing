/* eslint-disable */
import React from "react";

const FileSelect = ({ onChange, value, multiple= false }) => {
  return (
    <>
      <input
        type="file"
        id="myImage"
        name="myImage"
        accept="image/*"
        className="custom-file-input"
        multiple={multiple}
        onChange={onChange}
        style={{ display: "none" }}
      />
      <label htmlFor="myImage" className="custom-file-label">
        {value?.fileURL ? value?.name : "Choose File"}
      </label>
    </>
  );
};

export default FileSelect;
