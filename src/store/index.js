/* eslint-disable */
import { configureStore, MiddlewareArray } from "@reduxjs/toolkit";
import rootReducer from "./reducers";
import { createLogger } from "redux-logger";
import monitorReducersEnhancer from './enhancers/monitorReducer'

const logger = createLogger();

const store = configureStore({
  reducer: rootReducer,
  middleware: new MiddlewareArray().concat(logger),
  enhancers: [monitorReducersEnhancer]
});
if (process.env.NODE_ENV !== "production" && module.hot) {
  module.hot.accept("./reducers", () => store.replaceReducer(rootReducer));
}
export { store };
