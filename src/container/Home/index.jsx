/* eslint-disable */
import React, { useState } from "react";
import Card from "../../components/Card";
import { useSelector } from "react-redux";
import Modal from "../../components/Modal";
import { Icon } from "@iconify/react";
import MessageBox from "../../components/MessageBox";
import Header from "../../components/Header";


const Home = () => {
  const [message, setMessage] = useState("");
  const [type, setType] = useState("");
  const [values, setValues] = useState({});
  const [errors, setErrors] = useState({});
  const [id, setId] = useState("");
  const movieList = useSelector((state) => state?.movie?.movieList);
  const clear = () => {
    setValues({});
    setErrors({});
    setId();
    setMessage(null);
  };

  return (
    <>
      <MessageBox type={type} message={message} setMessage={setMessage} />
      <Modal
        id={id}
        setMessage={setMessage}
        setType={setType}
        setId={setId}
        values={values}
        setValues={setValues}
        errors={errors}
        setErrors={setErrors}
        clear={clear}
      />
      <Header Title={"Movie List"}>
        <button
          className="addBtn"
          onClick={() => window.$("#exampleModal").modal("show")}
        >
          <Icon icon="solar:add-circle-line-duotone" width="20" />
          Add Movie
        </button>
      </Header>
      <div className="card-category-2">
        {/* <span className="category-name">Movie List</span> <br /> */}

        <br />
        <ul>
          {movieList &&movieList.length ? [...movieList].sort((a, b) => b?.Id-a?.Id )?.map((e) => (
            <Card {...e} setId={setId} setValues={setValues} clear={clear} />
          )):'No Card'}
        </ul>
      </div>
    </>
  );
};
export default Home;
