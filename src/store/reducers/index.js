import { combineReducers } from 'redux'
import movieReducer from './movieSlice'

export default combineReducers({
    movie: movieReducer
})
