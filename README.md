# Movie Listing
1. Create a react application with following features.
   - Render list of data as card view. (4 or 5 cards in a row, successive cards should render on next rows)
   - Add a button at the top of the page for creating cards.
   - On clicking the button, open a modal and show a form to fill data.
   - Form contains following fields
      - [ ]    Title -> Textfield
      - [ ]    Description -> TextArea
      - [ ]    Duration -> Number
      - [ ]    Genre -> (dropdown)
    [Values => Action, Comedy, Drama, Fantasy, Horror, Mystery, Romance, Thriller]
    - Image => FileInput (Allow png or jpg)
    - On clicking the button, open a modal and show a form to fill data.
    -  After submitting the form, the page should be rerendered with a new card also added to top of available cards.
    - In a card, show the image as a background image. Then re render the other data like title, description, duration, genre in bottom of the card like Cards showing in Hotstar application. If the
    - description length is too long, ellipsis the description.
    - Each Card should be editable. So on hovering the card, the edit and delete icon should be visible
 and the user should be able to edit and delete using that icon. For editing, use the same creation
    - modal.
    - Also allow the user to click on each card, so that when clicked, please show a modal with view of
 each card data. (Just show each value like title, description, duration, genre ...)
2. Use Redux for store management. Use redux actions for adding, getting, updating and deleting from
redux store.
3. Create a public repo in github and add code inside the repo.
Notes:
❖ Use Typescript/Javascript (Give preference to Typescript).
❖ Use the latest stable version of Node.

## Getting Started with Create React App and Redux

```bash
# Clone this project
$ git clone https://gitlab.com/ArunMurali/movie-listing.git

# Access
$ cd movie-listing

# Install dependencies
$ npm install 

# Run the project
$ npm  start
# Runs the app in the development mode.\
# The server will initialize in the <https://localhost:3000/movie-listing>
```