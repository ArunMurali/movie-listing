/* eslint-disable */
import React from "react";

const Header = ({ Title, children }) => {
  return (
    <header className="header">
      {Title}
      {children}
    </header>
  );
};

export default Header;
