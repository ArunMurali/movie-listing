const message = "^Please fill the required fields";
export const schema = {
  Title: {
    presence: { allowEmpty: false, message },
  },
  Description: {
    presence: { allowEmpty: false, message },
  },
  Genre: {
    presence: { allowEmpty: false, message },
  },
  Duration: {
    presence: { allowEmpty: false, message },
  },
};
