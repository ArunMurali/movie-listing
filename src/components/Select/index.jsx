/* eslint-disable */
import React from "react";

const Select = ({ value, options = [], label, onChange, error }) => {
  return (
    <select
      value={value || ''}
      className={error ? "form-select red-border" : "form-select "}
      aria-label="Default select example"
      onChange={onChange}
    >
      <option key={''} value={""} >
        {label}
      </option>
      {options.map((e,i) => (
        <option key={i} value={e.value}>{e.label}</option>
      ))}
    </select>
  );
};

export default Select;
