/* eslint-disable */
import React, { useEffect} from "react";
import InputField from "../InputField";
import TextArea from "../TextArea";
import Select from "../Select";
import { Genre, MessageType } from "../../util/enum";
import { option } from "../../util/helper";
import { useDispatch, useSelector } from "react-redux";
import { addMovie, editMovie } from "../../store/reducers/movieSlice";
import { validate } from "validate.js";
import { schema } from "./schema";
import FileSelect from "../../components/FileSelect";

const Modal = (props) => {
  const {
    setMessage,
    setType,
    id,
    setId,
    setValues,
    values,
    setErrors,
    errors,
    clear,
  } = props;
  const dispatch = useDispatch();
  const movieList = useSelector((state) => state?.movie?.movieList);

  useEffect(() => {
    if (id) {
      if (movieList) {
        let data = movieList.find((e) => e.Id === id);
        setValues(data);
      }
    }
  }, [id, movieList,setValues]);

  const withNumberInputOnly = (func) => (field, value) => {
    const inputValue = value;
    const numericValue = inputValue.replace(/[^0-9]/g, "");
    func(field, numericValue);
  };

  const handleFieldChange = (field, value) => {
    if (field === "Image") {
      var reader = new FileReader();
      const file = value.file;
      if (file) {
        reader.readAsDataURL(file);
        reader.onload = function (event) {
          var dataURL = event.target.result;
          setValues((prev) => ({
            ...prev,
            [field]: { fileURL: dataURL, name: value.name },
          }));
        };
      }

      return;
    }
    setValues((prev) => ({ ...prev, [field]: value }));
  };
  const schemaValidate = () => {
    
    let error = validate(values, schema);
    let isValid = error ? true : false;
    setErrors(error ? error : {});
    if (isValid) {
      setMessage("Please fill the required fields");
      setType(MessageType.ERROR);
    }
    return !isValid;
  };

  const handleAdd = () => {
    const isValid = schemaValidate();
    if (isValid) {
      if (id) {
        dispatch(
          editMovie({
            ...values,
          })
        );
      } else {
        dispatch(
          addMovie({
            ...values,
          })
        );
      }

      setMessage(id ? "Movie Edited Successfully" : "Movie Added Successfully");
      setType(MessageType.SUCCESS);
      window.$("#exampleModal").modal("hide");
      setValues({});
      setId();
    }
  };

  return (
    <div
      className="modal fade"
      id="exampleModal"
      tabIndex="-1"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
      data-bs-backdrop="static"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id="exampleModalLabel">
              {id ? "Edit Movie" : "Add Movie"}
            </h1>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
              onClick={() => clear()}
            ></button>
          </div>
          <div className="modal-body">
            <form>
              <InputField
                id={"Title"}
                label={"Title"}
                value={values?.Title}
                error={errors?.Title}
                onChange={(e) => handleFieldChange("Title", e.target.value)}
              />
              <InputField
                id={"Duration"}
                label={"Duration (min)"}
                value={values?.Duration}
                error={errors?.Duration}
                maxLength={3}
                onChange={(e) => {
                  withNumberInputOnly(handleFieldChange)(
                    "Duration",
                    e.target.value
                  );
                }}
              />
              <Select
                options={option(Genre)}
                error={errors?.Genre}
                value={
                  option(Genre)?.find((e) => e.value === Number(values?.Genre))?.value 
                }
                onChange={(e) => handleFieldChange("Genre", e.target.value)}
                label={"Genre"}
              />
              <TextArea
                id={"Description"}
                label={"Description"}
                value={values?.Description}
                maxLength={300}
                error={errors?.Description}
                onChange={(e) =>
                  handleFieldChange("Description", e.target.value)
                }
              />
              <FileSelect
                onChange={(e) =>
                  handleFieldChange("Image", {
                    file: e.target.files[0],
                    name: e.target.files[0]?.name,
                  })
                }
                value={values?.Image}
              />
            </form>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
              onClick={() => {
                clear();
              }}
            >
              Close
            </button>
            <button
              type="button"
              onClick={handleAdd}
              className="btn btn-primary"
            >
              {id ? "Edit" : "Add"}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
