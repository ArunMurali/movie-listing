/* eslint-disable */
import React from "react";

const InputField = ({
  label,
  maxLength,
  onChange,
  type = "text",
  id,
  value,
  error,
}) => {
  return (
    <div className="mb-3">
      <label htmlFor={id} className="col-form-label">
        {label}
      </label>
      <input
        type={type}
        value={value || ''}
        className={error ? "form-control red-border" : "form-control"}
        onChange={onChange}
        id={id}
        maxLength={maxLength}
      />
    </div>
  );
};

export default InputField;
