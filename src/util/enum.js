export const Genre = {
  Action: 1,
  Comedy: 2,
  Drama: 3,
  Fantasy: 4,
  Horror: 5,
  Mystery: 6,
  Romance: 7,
  Thriller: 8,
  'Sci-Fi': 9
};
export const MessageType ={
  SUCCESS:1,
  ERROR:2
}
