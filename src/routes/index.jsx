import React from "react";
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  createRoutesFromElements,
} from "react-router-dom";
import Home from "../container/Home";

const Routes = () => {
  const router = createBrowserRouter(
    createRoutesFromElements(
        <Route index path="/movie-listing" element={<Home />} />
    )
  );
  return <RouterProvider router={router} />;
};

export default Routes;
