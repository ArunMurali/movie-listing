import { createSlice } from "@reduxjs/toolkit";
import {Interstellar, Shazam} from '../../assets/Images'
const initialState = {
  movieList: [
    {
      Id: 1,
      Title: "Interstellar ",
      Description:
        "Interstellar is a visually stunning sci-fi film where a team of astronauts embarks on a perilous space mission to save humanity from a dying Earth. Love, sacrifice, and the mysteries of the universe.",
      Image: {
        fileURL:Interstellar,
        name: "Interstellar",
      },
      Genre: 9,
      Duration: 169,
    },
    {
      Id: 2,
      Description:
        "Shazam is a fun superhero movie where a young boy transforms into a powerful adult hero with a single word. Full of humor, heart, and thrilling action, it explores his journey of discovering his powers, facing a villain, and embracing the true meaning of heroism.",
      Duration: "132",
      Genre: "1",

      Image: {
        fileURL:Shazam,
        name: "OIP.jpg",
      },
      Title: "Shazam!",
    },
  ],
};
export const counterSlice = createSlice({
  name: "movie",
  initialState,
  reducers: {
    addMovie: (state, action) => {
      const lastMovie = state.movieList[state.movieList.length - 1];
      state.movieList = [
        ...state.movieList,
        {
          ...action.payload,
          Id: (lastMovie?.Id || 0) + 1,
        },
      ];
    },
    deleteMovie: (state, action) => {
      
      state.movieList = state.movieList.filter((e) => e.Id !== action.payload);
    },
    editMovie: (state, action) => {
      state.movieList = state.movieList.map((e) =>
        e.Id === action.payload.Id ? { ...e, ...action.payload } : e
      );
    },
  },
});

export const { addMovie, deleteMovie, editMovie } = counterSlice.actions;
export default counterSlice.reducer;
