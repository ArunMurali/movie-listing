/* eslint-disable */
import React, { useState } from "react";
import { option, convertHrs } from "../../util/helper";
import { Genre } from "../../util/enum";
import { Icon } from "@iconify/react";
import { deleteMovie } from "../../store/reducers/movieSlice";
import { useDispatch } from "react-redux";
const Card = ({
  Title,
  Description,
  Image,
  Duration,
  Id,
  setId,
  setValues,
  clear,
  ...res
}) => {
  const [isFlipped, setIsFlipped] = useState(true);
  const MAX_LENGTH = 50;
  const isTextGreater = Description?.length > MAX_LENGTH;
  const dispatch = useDispatch();

  const handleFlip = () => {
    setIsFlipped(!isFlipped);
  };
  const truncateText = (text, maxLength) => {
    if (text.length <= maxLength) {
      return text;
    }
    return text.slice(0, maxLength) + "...";
  };

  return (
    <li key={Id}>
      <div className={`img-card iCard-style1`}>
        <div className="card-content">
          <span className="card-manage">
            <Icon
              icon="lucide:edit"
              width="20"
              color="#FF9800"
              onClick={() => {
                setId(Id);
                window.$("#exampleModal").modal("show");
              }}
            />
            <Icon
              icon="material-symbols:delete-outline"
              color="#d83a40"
              width="20"
              onClick={() => {
                dispatch(deleteMovie(Id));
                clear();
              }}
            />
          </span>
          <div className="card-image">
            {/* <span className="card-title">{Title}</span> */}
            <img src={Image?.fileURL} alt={Title} />
          </div>
          <p className="movie-info">
            <h2 className="movie-title">{Title}</h2>
            <span className="movie-Genre">
              {option(Genre)?.find((e) => e.value === Number(res.Genre))?.label}
            </span>
            <span className="separator">|</span>
            <span className="movie-duration">{convertHrs(Duration)}</span>
          </p>
          <div className={`card-text`}>
            {isFlipped && isTextGreater ? (
              <p>{truncateText(Description, MAX_LENGTH)}</p>
            ) : (
              <p>{Description}</p>
            )}
          </div>
        </div>

        <div className="card-link">
          <a onClick={handleFlip} href="javascript:void(0)" title="Read Full">
            <span>
              {isTextGreater &&
                (isFlipped ? (
                  "Read Full Description"
                ) : (
                  <Icon icon="ic:baseline-arrow-back" width="20" />
                ))}
            </span>
          </a>
        </div>
      </div>
    </li>
  );
};

export default Card;
