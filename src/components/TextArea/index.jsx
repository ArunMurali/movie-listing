/* eslint-disable */
import React, { useState } from "react";

const TextArea = ({ label, maxLength, onChange, type = "text", id, value, error }) => {
  const [wordCount, setWordCount] = useState(0);
  const handleInputChange = (e) => {
    const text = e.target.value;
    if (text) {
      const words = text.split("");
      const currentWordCount = words.length;
      if (currentWordCount <= maxLength) {
        setWordCount(currentWordCount);
        onChange(e);
      }
    }
  };

  return (
    <div className="mb-3">
      <label htmlFor={id} className="col-form-label">
        {label}
        <span></span>
      </label>
      <div className="textarea-container">
        <textarea
          type={type}
          value={value || ''}
          className={error?"form-control red-border":"form-control"}
          onChange={handleInputChange}
          id={id}
          maxLength={maxLength}
        />
        <span className="word-count">
          {wordCount}/{maxLength} words
        </span>
      </div>
    </div>
  );
};

export default TextArea;
