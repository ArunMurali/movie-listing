import React from "react";
import "./scss/App.scss";
import Routes from "./routes";

function App() {
  return <Routes />;
}

export default App;
