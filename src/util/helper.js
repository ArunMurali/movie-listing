export const option = (Genre) =>
  Object.keys(Genre).map((e) => ({
    value: Genre[e],
    label: e,
  }));
export const convertHrs = (minutes) => {
  if (minutes) {
    const hours = Math.floor(minutes / 60);
    const remainingMinutes = minutes % 60;
    return `${hours}hrs ${remainingMinutes}min`;
  }
  return "0 hrs";
};
